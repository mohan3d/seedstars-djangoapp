from django.db import models


class Account(models.Model):
    """Represents account entry name/email"""
    name = models.CharField(max_length=255)
    email = models.EmailField()

    def __str__(self):
        return self.name
