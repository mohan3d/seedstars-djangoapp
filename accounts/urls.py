from django.urls import path
from . import views

app_name = 'accounts'

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('list/', views.AccountListView.as_view(), name='list'),
    path('add/', views.AccountCreateView.as_view(), name='new'),
]
