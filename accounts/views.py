from django.views import generic
from django.urls import reverse_lazy

from .forms import AccountForm
from .models import Account


class HomePageView(generic.TemplateView):
    template_name = 'accounts/home.html'


class AccountListView(generic.ListView):
    model = Account
    context_object_name = 'accounts_list'
    template_name = 'accounts/account_list.html'


class AccountCreateView(generic.CreateView):
    form_class = AccountForm
    template_name = 'accounts/account_form.html'
    success_url = reverse_lazy('accounts:list')
